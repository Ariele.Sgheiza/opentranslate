package cui.unige.opentranslate;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.translate.*;
import com.google.cloud.translate.Language;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;

import org.apache.commons.codec.language.bm.Lang;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class LangSelection extends AppCompatActivity {
    ListView listView;
    Translate translate;
    String mLang[];
    public static String code = "";
    public static String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lang_selection);
        listView = findViewById(R.id.listView);

        getTranslateService();
        //Lista Lingue di Google Translate
        List<Language> lang = translate.listSupportedLanguages();

        MyAdapter adapter = new MyAdapter(this, lang);
        listView.setAdapter(adapter);

        //Selezione lingua
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                code = lang.get(position).getCode();
                name = lang.get(position).getName();

                Intent i = new Intent(LangSelection.this, MainActivity.class);
                i.putExtra("code", code);
                i.putExtra("name", name);
                startActivity(i);
            }
        });

    }

    public void getTranslateService()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try (InputStream is = getResources().openRawResource(R.raw.credentials)) {

            //Get credentials:
            final GoogleCredentials myCredentials = GoogleCredentials.fromStream(is);

            //Set credentials and get translate service:
            TranslateOptions translateOptions = TranslateOptions.newBuilder().setCredentials(myCredentials).build();
            translate = translateOptions.getService();

        } catch (IOException ioe) {
            ioe.printStackTrace();

        }
    }

    class MyAdapter extends ArrayAdapter<String>{
        Context context;
        List<Language> rLang;

        MyAdapter(Context c, List lang){
            super(c, R.layout.row, R.id.listView, lang);
            this.context=c;
            this.rLang = lang;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            TextView thisLang = row.findViewById(R.id.textView1);

            thisLang.setText(rLang.get(position).getName());

            return row;
        }
    }
}