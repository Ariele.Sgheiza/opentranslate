package cui.unige.opentranslate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.fonts.Font;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Net;
import org.opencv.imgproc.Imgproc;
import org.opencv.dnn.Dnn;
import org.opencv.utils.Converters;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.translate.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity<Graphics2D> extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    //riceve frame dalla camera
    CameraBridgeViewBase cameraBridgeViewBase;
    BaseLoaderCallback baseLoaderCallback;

    Translate translate;
    String translatedText;
    private Button langButton;
    String tinyYoloCfg = Environment.getExternalStorageDirectory()+"/dnns/yolov3-tiny.cfg";
    String tinyYoloWeights=Environment.getExternalStorageDirectory()+"/dnns/yolov3-tiny.weights";
    //String tinyYoloCfg = "file:///android_asset/yolov3-tiny.cfg";
    //String tinyYoloWeights = "file:///android_asset/yolov3-tiny.weights";
    Net tinyYolo;
    int x;
    int y;
    public Font font;
    //private ImageView imageTradu;

    //test per fare onTouchEvent
    Mat frame;
    int[] ind;
    Rect[] boxesArray;
    List<Integer> listid;

    //test NMS
    int ArrayLenght;
    List<Float> listconf;
    List<Rect> listrect;
    float confThreshold;

    public void LangSelection(){
        Intent intent = new Intent(this, LangSelection.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        OpenCVLoader.initDebug();

        //test mettere file in asset, non funziona
        AssetManager assetManagerTinyCfg = getAssets();
        try {
            assetManagerTinyCfg.open("yolov3-tiny.cfg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //tinyYoloCfg = getApplicationContext().getFilesDir().getAbsolutePath();

        tinyYolo = Dnn.readNetFromDarknet(tinyYoloCfg, tinyYoloWeights);

        langButton = findViewById(R.id.langButton);
        langButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LangSelection();
            }
        });

        if(LangSelection.name!=""){
            langButton = findViewById(R.id.langButton);
            langButton.setText(LangSelection.name);
        }

        //mettere i frame nella CameraView di xml
        cameraBridgeViewBase = (JavaCameraView)findViewById(R.id.CameraView);
        cameraBridgeViewBase.setVisibility(SurfaceView.VISIBLE);
        cameraBridgeViewBase.setCvCameraViewListener(this);

        //controlla se ci sono errori
        baseLoaderCallback = new BaseLoaderCallback(this) {
            @Override
            public void onManagerConnected(int status) {
                super.onManagerConnected(status);
                switch (status){
                    case BaseLoaderCallback.SUCCESS:
                        cameraBridgeViewBase.enableView();
                        break;
                    default:
                        super.onManagerConnected(status);
                        break;
                }
            }
        };
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        //prende il frame
        frame = inputFrame.rgba();
        //Core.flip(frame, frame, 1);

        Imgproc.cvtColor(frame, frame, Imgproc.COLOR_RGBA2RGB);

        //0.00392 = 1/255, 255 sono i colori/pixel, è scalefactor, permette di convertire i valori dei colori in percentuali es: [255, 0, 255] * 1/255 = [1, 0, 1]
        //preprocess frame in blob cambiando forma per ottimizzarla per tinyYolo
        //Scalar serve in alcuni casi per sottrarre pixel
        Mat imageBlob = Dnn.blobFromImage(frame, 0.00392, new Size(416, 416), new Scalar(0, 0, 0)); //manca swapRB = false e crop=false

        tinyYolo.setInput(imageBlob);

        java.util.List<Mat> layers = new java.util.ArrayList<Mat>(2); //layers list Matrice, verrà riempita con i layers Yolo

        List<String> layersNames = new java.util.ArrayList<>(); //crei lista e metti dentro i due layers
        layersNames.add(0, "yolo_16"); //2 tinyYolo layers
        layersNames.add(1, "yolo_23");
        //per Big Yolo devi aggiungere altri layers

        tinyYolo.forward(layers, layersNames);

        confThreshold = 0.2f;
        listid = new ArrayList<>(); //Lista di Ids
        listconf = new ArrayList<>(); //Lista di confidence
        listrect = new ArrayList<>(); //Lista duî rectangles, riceve coordinate box

        for (int i = 0; i < layers.size(); ++i) //iterate la lista layers, quindi è flessibile per tinyyolo o yolo normale
        {
            Mat level = layers.get(i);
            for (int j = 0; j < level.rows(); ++j) //iterate ogni linea del layer
            {
                Mat row = level.row(j);
                Mat scores = row.colRange(5, level.cols()); //togli i primi 5 neurons, 4 per bbox e un altro per objectness score (probabilità che c'é qualcosa) rimangono CocoClasses
                Core.MinMaxLocResult minMaxScore = Core.minMaxLoc(scores); //maximum scores di un oggetto per poi definire confidence

                float confidence = (float) minMaxScore.maxVal;

                Point classIdPoint = minMaxScore.maxLoc; //è la location massima

                if (confidence > confThreshold) //allora lo processi
                {
                    //CAMBIARE CENTRO E LATI
                    int centerX = (int) (row.get(0, 0)[0] * frame.cols()); //prendi i valori che ha trovato yolo della posizione dell'oggetto (in percentuale) e lo trasfromi in pixel
                    int centerY = (int) (row.get(0, 1)[0] * frame.rows());
                    int width = (int) (row.get(0, 2)[0] * frame.cols());
                    int height = (int) (row.get(0, 3)[0] * frame.rows());

                    int left = centerX - width / 2;
                    int top = centerY - height / 2;

                    listid.add((int) classIdPoint.x);
                    listconf.add((float) confidence);
                    listrect.add(new Rect(left, top, width, height)); //costruisci oggetto Rectangle, left, top, right, bottom
                }
            }
        }
        ArrayLenght = listconf.size(); //quante rows sono riuscite a passare nella confidence, quindi sono state riconosciute

            if (ArrayLenght >= 1) {
                //non maximum suppression, permette di non creare una doppia box sullo stesso oggetto
                float nmsThersh = 0.2f;
                MatOfFloat confidences = new MatOfFloat(Converters.vector_float_to_Mat(listconf));
                boxesArray = listrect.toArray(new Rect[0]);
                MatOfRect boxes = new MatOfRect(boxesArray);
                MatOfInt indices = new MatOfInt(); //lista di indici di ids, listconf et listrect

                Dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThersh, indices); //metodo di OpenCV per NMS

                ind = indices.toArray();

                for (int i = 0; i < ind.length; ++i) {
                    int idx = ind[i]; //prendi un indice e trovi il rect, il box et il listconf
                    Rect box = boxesArray[idx];
                    int idCoco = listid.get(idx);
                    float conf = listconf.get(idx);

                    Point position = new Point(x, y);

                    if (box.contains(position)) {

                            List<String> cocoNames = Arrays.asList("a person", "a bicycle", "a car", "a motorbike", "an airplane", "a bus", "a train", "a truck", "a boat", "a traffic light", "a fire hydrant", "a stop sign", "a parking meter", "a bench", "a bird", "a cat", "a dog", "a horse", "a sheep", "a cow", "an elephant", "a bear", "a zebra", "a giraffe", "a backpack", "an umbrella", "a handbag", "a tie", "a suitcase", "a frisbee", "skis", "a snowboard", "a sports ball", "a kite", "a baseball bat", "a baseball glove", "a skateboard", "a surfboard", "a tennis racket", "a bottle", "a wine glass", "a cup", "a fork", "a knife", "a spoon", "a bowl", "a banana", "an apple", "a sandwich", "an orange", "broccoli", "a carrot", "a hot dog", "a pizza", "a doughnut", "a cake", "a chair", "a sofa", "a potted plant", "a bed", "a dining table", "a toilet", "a TV monitor", "a laptop", "a computer mouse", "a remote control", "a keyboard", "a cell phone", "a microwave", "an oven", "a toaster", "a sink", "a refrigerator", "a book", "a clock", "a vase", "a pair of scissors", "a teddy bear", "a hair drier", "a toothbrush");

                            //int intConf = (int) (conf * 100);

                            //Traduzione
                            String toTranslate = cocoNames.get(idCoco); //prende id dell'oggetto nella lista cocoNames
                            getTranslateService();
                            String testo = translate(toTranslate);

                            //Creazione immagine con traduzione (white.png)
                            Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.white2);
                            Bitmap tradut = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
                            String text = testo;
                            Canvas cs = new Canvas(tradut);
                            Paint paint = new Paint();
                            paint.setTextSize(120);
                            paint.setColor(Color.BLACK);
                            paint.setStyle(Paint.Style.FILL);
                            cs.drawBitmap(image, 0f, 0f, null);
                            float height = paint.measureText("yY");
                            float width = paint.measureText(text);
                            float x_coord = (image.getWidth() - width)/2;
                            cs.drawText(text, x_coord, height+15f, paint);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ImageView imageTradu = findViewById(R.id.imageTradu);
                                    imageTradu.setImageBitmap(tradut);
                                    imageTradu.setVisibility(View.VISIBLE);
                                }
                            });

                            Imgproc.putText(frame, toTranslate, box.tl(), Core.FONT_ITALIC, 2, new Scalar(0, 0, 0), 7);
                            Imgproc.putText(frame, toTranslate, box.tl(), Core.FONT_ITALIC, 2, new Scalar(255, 255, 255), 3);

                            Imgproc.rectangle(frame, box.tl(), box.br(), new Scalar(0, 255, 0), 2);

                            System.out.println(box);
                    }

                }

            }
        return frame;
    }

    public void getTranslateService()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try (InputStream is = getResources().openRawResource(R.raw.credentials)) {

            //Prendi credentials:
            final GoogleCredentials myCredentials = GoogleCredentials.fromStream(is);

            //Set credentials and get translate service:
            TranslateOptions translateOptions = TranslateOptions.newBuilder().setCredentials(myCredentials).build();
            translate = translateOptions.getService();

        } catch (IOException ioe) {
            ioe.printStackTrace();

        }
    }

    public String translate(String testo) {

        //Get input text to be translated:
        //originalText = toTranslate.toString();
        //Bundle getCode = getIntent().getExtras();
        if(LangSelection.code == ""){
            Translation translation = translate.translate(testo, Translate.TranslateOption.targetLanguage("en"), Translate.TranslateOption.model("base"));
            translatedText = translation.getTranslatedText();
        }else{
            Translation translation = translate.translate(testo, Translate.TranslateOption.targetLanguage(LangSelection.code), Translate.TranslateOption.model("base"));
            translatedText = translation.getTranslatedText();
        }
        return translatedText;
        //Translated text and original text are set to TextViews:
        //translatedTv.setText(translatedText);

    }

    //Test onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        x = (int) event.getX();
        y = (int) event.getY();
        //Toast.makeText(MainActivity.this, "coordinate: " + x + ", " + y + ".", Toast.LENGTH_LONG).show();
        //---------------------------------------------------------------

       /* if (ArrayLenght >= 1) {
            //non maximum suppression, permette di non creare una doppia box sullo stesso oggetto
            float nmsThersh = 0.2f;
            MatOfFloat confidences = new MatOfFloat(Converters.vector_float_to_Mat(listconf));
            boxesArray = listrect.toArray(new Rect[0]);
            MatOfRect boxes = new MatOfRect(boxesArray);
            MatOfInt indices = new MatOfInt(); //lista di indici di ids, listconf et listrect

//            Dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThersh, indices);

            //Draw layers boxes
            ind = indices.toArray();



        for (int i = 0; i < ind.length; ++i) {
            int idx = ind[i]; //prendi un indice e trovi il rect, il box et il confs
            Rect box = boxesArray[idx];
            int idCoco = listid.get(idx);
            //float conf = confs.get(idx);

            Point position = new Point(x, y);

            if (box.contains(position)) {

                List<String> cocoNames = Arrays.asList("a person", "a bicycle", "a motorbike", "an airplane", "a bus", "a train", "a truck", "a boat", "a traffic light", "a fire hydrant", "a stop sign", "a parking meter", "a car", "a bench", "a bird", "a cat", "a dog", "a horse", "a sheep", "a cow", "an elephant", "a bear", "a zebra", "a giraffe", "a backpack", "an umbrella", "a handbag", "a tie", "a suitcase", "a frisbee", "skis", "a snowboard", "a sports ball", "a kite", "a baseball bat", "a baseball glove", "a skateboard", "a surfboard", "a tennis racket", "a bottle", "a wine glass", "a cup", "a fork", "a knife", "a spoon", "a bowl", "a banana", "an apple", "a sandwich", "an orange", "broccoli", "a carrot", "a hot dog", "a pizza", "a doughnut", "a cake", "a chair", "a sofa", "a potted plant", "a bed", "a dining table", "a toilet", "a TV monitor", "a laptop", "a computer mouse", "a remote control", "a keyboard", "a cell phone", "a microwave", "an oven", "a toaster", "a sink", "a refrigerator", "a book", "a clock", "a vase", "a pair of scissors", "a teddy bear", "a hair drier", "a toothbrush");

                //int intConf = (int) (conf * 100);

                //Traduzione
                String toTranslate = cocoNames.get(idCoco); //prende id dell'oggetto nella lista cocoNames
                getTranslateService();
                String testo = translate(toTranslate);

                //Creazione immagine con traduzione (white.png)
                Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.white2);
                Bitmap tradut = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
                String text = testo;
                Canvas cs = new Canvas(tradut);
                Paint paint = new Paint();
                paint.setTextSize(120);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.FILL);
                cs.drawBitmap(image, 0f, 0f, null);
                float height = paint.measureText("yY");
                float width = paint.measureText(text);
                float x_coord = (image.getWidth() - width) / 2;
                cs.drawText(text, x_coord, height + 15f, paint);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ImageView imageTradu = findViewById(R.id.imageTradu);
                        imageTradu.setImageBitmap(tradut);
                        imageTradu.setVisibility(View.VISIBLE);
                    }
                });

                Imgproc.putText(frame, toTranslate, box.tl(), Core.FONT_ITALIC, 2, new Scalar(0, 0, 0), 7);
                Imgproc.putText(frame, toTranslate, box.tl(), Core.FONT_ITALIC, 2, new Scalar(255, 255, 255), 3);

                Imgproc.rectangle(frame, box.tl(), box.br(), new Scalar(0, 255, 0), 2);

                System.out.println(box);
            } else {
                Toast.makeText(MainActivity.this, "No object found!", Toast.LENGTH_LONG).show();
            }

        }
    }*/

        //---------------------------------------------------------------
        return false;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
            //String tinyYoloCfg = Environment.getExternalStorageDirectory()+"/dnns/yolov3-tiny.cfg";
            //String tinyYoloWeights = Environment.getExternalStorageDirectory()+"/dnns/yolov3-tiny.weights";
            tinyYolo = Dnn.readNetFromDarknet(tinyYoloCfg, tinyYoloWeights);
    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()){
            Toast.makeText(getApplicationContext(),"Error", Toast.LENGTH_LONG).show();
        } else {
            baseLoaderCallback.onManagerConnected(baseLoaderCallback.SUCCESS);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(cameraBridgeViewBase!=null){
            cameraBridgeViewBase.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(cameraBridgeViewBase!=null){
            cameraBridgeViewBase.disableView();
        }
    }
}